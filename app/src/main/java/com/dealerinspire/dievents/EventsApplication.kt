package com.dealerinspire.dievents

import android.app.Application
import com.dealerinspire.dievents.dagger.AppComponent
import com.dealerinspire.dievents.dagger.AppModule
import com.dealerinspire.dievents.dagger.DaggerAppComponent
import timber.log.Timber

class EventsApplication: Application() {

    private var applicationComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()

        initializeInjector()
        getApplicationComponent().inject(this)

        Timber.plant(Timber.DebugTree())
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Dagger2
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    fun initializeInjector() {
        this.applicationComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    fun getApplicationComponent(): AppComponent {
        return this.applicationComponent!!
    }

}