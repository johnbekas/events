package com.dealerinspire.dievents.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtils {
    public static final String ISO8601DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String USER_DATETIME_FORMAT = "MMM d, yyyy, h:mm a";
    public static final String USER_TIME_FORMAT = "h:mm a";
    public static final String USER_DATE_FORMAT = "MMM d, yyyy";

    public static boolean isToday(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.US);
        Date now = new Date();
        return sdf.format(now).equals(sdf.format(date));
    }

    public static String getFormattedDateTime(Date date, boolean hideDateIfToday) {
        if (hideDateIfToday && isToday(date)) {
            SimpleDateFormat sdf = new SimpleDateFormat(USER_TIME_FORMAT, Locale.US);
            return sdf.format(date);
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat(USER_DATETIME_FORMAT, Locale.US);
            return sdf.format(date);
        }
    }

    public static String getFormattedDate(Date date) {
        if (isToday(date)) {
            return "Today";
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat(USER_DATE_FORMAT, Locale.US);
            return sdf.format(date);
        }
    }

    public static String getFormattedTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(USER_TIME_FORMAT, Locale.US);
        return sdf.format(date);
    }

    public static Date convertAtomDateStringToDate(String timeString) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(ISO8601DATEFORMAT, Locale.US);
        return sdf.parse(timeString);
    }

    public static String convertDateToAtomDateString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(ISO8601DATEFORMAT, Locale.US);
        return sdf.format(date);
    }

    public static String getAtomTimestamp() {
        return convertDateToAtomDateString(new Date());
    }
}
