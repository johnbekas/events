package com.dealerinspire.dievents.utils;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.RawRes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class RawUtils {
    public static String getRawJsonAsString(Context context, @RawRes int resId)
            throws IOException, NullPointerException, Resources.NotFoundException {

        InputStream in_s = context.getResources().openRawResource(resId);
        byte[] b = new byte[in_s.available()];
        in_s.read(b);

        return new String(b);
    }

    public static JSONObject getRawJson(Context context, @RawRes int resId)
            throws IOException, NullPointerException, Resources.NotFoundException, JSONException {

        return new JSONObject(getRawJsonAsString(context, resId));
    }

    public static JSONArray getRawJsonArray(Context context, @RawRes int resId)
            throws IOException, NullPointerException, Resources.NotFoundException, JSONException {

        return new JSONArray(getRawJsonAsString(context, resId));
    }
}
