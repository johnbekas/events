package com.dealerinspire.dievents.utils;

import android.app.Activity;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;

public class ImageUtils {

    @Nullable
    public static Drawable getImageDrawable(@DrawableRes int resourceId, Activity activity)
            throws NotFoundException {

        if (activity != null) {
            if (Build.VERSION.SDK_INT >= 21) {
                return activity.getResources().getDrawable(resourceId, activity.getTheme());
            } else {
                //noinspection deprecation
                return activity.getResources().getDrawable(resourceId);
            }
        } else {
            return null;
        }
    }
}
