package com.dealerinspire.dievents.controller

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferenceController @Inject constructor(private val context: Context) {

    private val appPreferences: SharedPreferences

    init {
        appPreferences = context.getSharedPreferences("EventsPreferences", Context.MODE_PRIVATE)
    }

    fun isFavorite(eventId: Long): Boolean {
        val set = appPreferences.getStringSet(FAVORITES, mutableSetOf())
        return set.contains(eventId.toString())
    }

    fun addFavorite(eventId: Long) {
        val set = appPreferences.getStringSet(FAVORITES, mutableSetOf())
        set.add(eventId.toString())
        appPreferences.edit().putStringSet(FAVORITES, set).apply()
    }

    fun removeFavorite(eventId: Long) {
        val set = appPreferences.getStringSet(FAVORITES, mutableSetOf())
        set.remove(eventId.toString())
        appPreferences.edit().putStringSet(FAVORITES, set).apply()
    }

    companion object {
        val FAVORITES = "FAVORITES"
    }

}
