package com.dealerinspire.dievents.model

import android.content.Context
import com.dealerinspire.dievents.R
import com.dealerinspire.dievents.api.EventResponse
import com.dealerinspire.dievents.utils.RawUtils
import com.google.gson.Gson
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Session @Inject constructor(private val context: Context) {

    var events: List<Event> = ArrayList<Event>()
    var locations: List<Location> = ArrayList<Location>()
    var speakers: List<Speaker> = ArrayList<Speaker>()

    fun loadData() {
        val eventsJson = RawUtils.getRawJsonAsString(context, R.raw.events)

        Timber.d(eventsJson);

        val eventResponse = Gson().fromJson<EventResponse>(eventsJson, EventResponse::class.java)

        Timber.d(eventResponse.toString());

        this.speakers = eventResponse.speakers
        this.locations = eventResponse.locations
        val eventList: MutableList<Event> = ArrayList<Event>();

        val speakerMap = HashMap<Long, Speaker>()
        for (s in eventResponse.speakers) {
            speakerMap.put(s.id, s)
        }

        for (eventResp in eventResponse.events) {
            var location: Location? = null
            val speakers: MutableList<Speaker> = ArrayList<Speaker>()

            for (l in eventResponse.locations) {
                if (l.id == eventResp.locationId) {
                    location = l;
                }
            }

            for (speakerId in eventResp.speakerIds) {
                val speaker = speakerMap.get(speakerId)
                if (speaker != null) {
                    speakers.add(speaker)
                }
            }

            val event = Event(id = eventResp.id,
                    name = eventResp.name,
                    description = eventResp.description,
                    location = location ?: Location(),
                    speakers = speakers,
                    startDateAtom = eventResp.startDateAtom,
                    endDateAtom = eventResp.endDateAtom
            )

            Timber.d(event.toString())
            eventList.add(event)
        }

        this.events = eventList
    }

    fun getEvent(eventId: Long?): Event {
        if (eventId == null) {
            return Event()
        }

        for (event in events) {
            if (event.id == eventId) {
                return event
            }
        }

        return Event()
    }
}