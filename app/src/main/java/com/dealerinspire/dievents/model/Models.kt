package com.dealerinspire.dievents.model

import com.dealerinspire.dievents.utils.DateTimeUtils
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

data class Event(val id: Long = 0,
                 val name: String = "",
                 val description: String = "",
                 private val startDateAtom: String = "",
                 private val endDateAtom: String = "",
                 val location: Location = Location(),
                 val speakers: List<Speaker> = ArrayList<Speaker>()) {

    val startDate: Date?
    val endDate: Date?

    init {
        var sd: Date?
        var ed: Date?
        try {
            sd = DateTimeUtils.convertAtomDateStringToDate(startDateAtom)
        } catch (e: Exception) {
            Timber.e(e)
            sd = null;
        }
        startDate = sd

        try {
            ed = DateTimeUtils.convertAtomDateStringToDate(endDateAtom)
        } catch (e: Exception) {
            Timber.e(e)
            ed = null;
        }
        endDate = ed
    }

    fun getDateTimeString(): String {
        try {
            val START_FORMAT = "MMMM d, h:mm a"
            val END_FORMAT = "h:mm a"
            val sdf1 = SimpleDateFormat(START_FORMAT, Locale.US)
            val sdf2 = SimpleDateFormat(END_FORMAT, Locale.US)

            return String.format("%s - %s", sdf1.format(startDate), sdf2.format(endDate))
        } catch (e: Exception) {
            Timber.e(e);
            return ""
        }
    }
}

data class Speaker(val id: Long = 0,
                   val name: String = "",
                   val url: String = "")

data class Location(val id: Long = 0,
                    val name: String = "")