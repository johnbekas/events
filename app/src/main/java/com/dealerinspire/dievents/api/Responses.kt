package com.dealerinspire.dievents.api

import com.dealerinspire.dievents.model.Location
import com.dealerinspire.dievents.model.Speaker

data class EventResponse(val speakers: List<Speaker> = ArrayList<Speaker>(),
                         val locations: List<Location> = ArrayList<Location>(),
                         val events: List<EventResp> = ArrayList<EventResp>())

data class EventResp(val id: Long = 0,
                     val name: String = "",
                     val description: String = "",
                     val startDateAtom: String = "",
                     val endDateAtom: String = "",
                     val locationId: Long = 0,
                     val speakerIds: List<Long> = ArrayList<Long>())

