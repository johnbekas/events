package com.dealerinspire.dievents.dagger

import android.content.Context
import com.dealerinspire.dievents.EventsApplication
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: EventsApplication) {

    @Provides
    @Singleton
    internal fun provideApplicationContext(): Context {
        return this.application
    }

    @Provides
    @Singleton
    fun providesPicasso(): Picasso {
        val picasso = Picasso.with(application)
        picasso.setLoggingEnabled(true)

        return picasso
    }
}
