package com.dealerinspire.dievents.dagger

import android.content.Context

import com.dealerinspire.dievents.EventsApplication
import com.dealerinspire.dievents.view.EventDetailActivity
import com.dealerinspire.dievents.view.EventListActivity
import com.dealerinspire.dievents.view.MainActivity
import com.dealerinspire.dievents.view.SpeakerListActivity

import javax.inject.Singleton

import dagger.Component

@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    fun inject(application: EventsApplication)

    fun inject(activity: EventDetailActivity)
    fun inject(activity: EventListActivity)
    fun inject(activity: MainActivity)
    fun inject(activity: SpeakerListActivity)

    //Exposed to sub-graphs.
    fun context(): Context
}