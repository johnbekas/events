package com.dealerinspire.dievents.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.dealerinspire.dievents.EventsApplication
import com.dealerinspire.dievents.R
import com.dealerinspire.dievents.model.Session
import com.dealerinspire.dievents.model.Speaker
import com.dealerinspire.dievents.view.widget.VerticalSpaceItemDecoration
import com.squareup.picasso.Picasso
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class SpeakerListActivity : AppCompatActivity() {

    @Inject @JvmField var picasso: Picasso? = null
    @Inject @JvmField var session: Session? = null

    @BindView(R.id.speaker_list) @JvmField internal var speakerListView: RecyclerView? = null

    private val speakerAdapter = SpeakerAdapter()
    private val speakerList = Collections.synchronizedList(ArrayList<Speaker>())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_speaker_list)

        (this.applicationContext as EventsApplication).getApplicationComponent().inject(this)

        ButterKnife.bind(this)

        speakerList.clear()
        speakerList.addAll(session?.speakers as List<Speaker>)

        if (speakerListView != null) {
            speakerListView!!.setLayoutManager(LinearLayoutManager(this))
            speakerListView!!.setAdapter(speakerAdapter)
            speakerListView!!.addItemDecoration(VerticalSpaceItemDecoration(8))
            speakerListView!!.setNestedScrollingEnabled(false)
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Message RecyclerView Adapter and View Holder
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    class SpeakerViewHolder internal constructor(inflater: LayoutInflater, parent: ViewGroup) : RecyclerView.ViewHolder(inflater.inflate(R.layout.template_speaker, parent, false)) {

        @BindView(R.id.name) @JvmField internal var name: TextView? = null
        @BindView(R.id.avatar) @JvmField internal var avatar: ImageView? = null

        init {

            ButterKnife.bind(this, itemView)
        }
    }

    private inner class SpeakerAdapter : RecyclerView.Adapter<SpeakerViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpeakerViewHolder {
            return SpeakerViewHolder(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: SpeakerViewHolder, position: Int) {
            var speaker: Speaker? = null

            synchronized(speakerList) {
                speaker = speakerList.get(position)
            }

            //hideError()

            val url: String = speaker?.url ?: ""
            Timber.d(speaker.toString())
            picasso!!.load(url).into(holder.avatar)

            holder.name?.text = speaker?.name
        }

        override fun getItemCount(): Int {
            synchronized(speakerList) {
                return speakerList.size
            }
        }
    }

}
