package com.dealerinspire.dievents.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import butterknife.ButterKnife
import butterknife.OnClick
import com.dealerinspire.dievents.EventsApplication
import com.dealerinspire.dievents.R
import com.dealerinspire.dievents.api.EventResponse
import com.dealerinspire.dievents.model.Event
import com.dealerinspire.dievents.model.Location
import com.dealerinspire.dievents.model.Session
import com.dealerinspire.dievents.model.Speaker
import com.dealerinspire.dievents.utils.RawUtils
import com.google.gson.Gson
import timber.log.Timber
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject @JvmField var session: Session? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (this.applicationContext as EventsApplication).getApplicationComponent().inject(this)

        ButterKnife.bind(this)

        session?.loadData()
    }

    @OnClick(R.id.events_layout)
    fun onEventsClicked() {
        val intent = Intent(this, EventListActivity::class.java)
        startActivity(intent)
    }

    @OnClick(R.id.speakers_layout)
    fun onSpeakersClicked() {
        val intent = Intent(this, SpeakerListActivity::class.java)
        startActivity(intent)
    }
}
