package com.dealerinspire.dievents.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.dealerinspire.dievents.Constants
import com.dealerinspire.dievents.EventsApplication
import com.dealerinspire.dievents.R
import com.dealerinspire.dievents.model.Event
import com.dealerinspire.dievents.model.Session
import com.dealerinspire.dievents.view.widget.VerticalSpaceItemDecoration
import com.squareup.picasso.Picasso
import timber.log.Timber
import javax.inject.Inject

class EventListActivity : AppCompatActivity() {

    @Inject @JvmField var picasso: Picasso? = null
    @Inject @JvmField var session: Session? = null

    @BindView(R.id.speaker_list) @JvmField internal var speakerListView: RecyclerView? = null

    private val eventAdapter = EventAdapter()
    private val eventList: MutableList<Event> = mutableListOf(Event())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_speaker_list)

        (this.applicationContext as EventsApplication).getApplicationComponent().inject(this)

        ButterKnife.bind(this)

        eventList.clear()
        eventList.addAll(session?.events as List<Event>)

        if (speakerListView != null) {
            speakerListView!!.setLayoutManager(LinearLayoutManager(this))
            speakerListView!!.setAdapter(eventAdapter)
            speakerListView!!.addItemDecoration(VerticalSpaceItemDecoration(8))
            speakerListView!!.setNestedScrollingEnabled(false)
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Message RecyclerView Adapter and View Holder
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////

    class EventViewHolder internal constructor(inflater: LayoutInflater, parent: ViewGroup) : RecyclerView.ViewHolder(inflater.inflate(R.layout.template_event, parent, false)) {

        @BindView(R.id.container) @JvmField internal var container: ViewGroup? = null
        @BindView(R.id.date_and_time) @JvmField internal var dateAndTime: TextView? = null
        @BindView(R.id.track_indicator) @JvmField internal var trackIndicator: View? = null
        @BindView(R.id.name) @JvmField internal var name: TextView? = null
        @BindView(R.id.room_name) @JvmField internal var roomName: TextView? = null

        init {

            ButterKnife.bind(this, itemView)
        }
    }

    private inner class EventAdapter : RecyclerView.Adapter<EventViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
            return EventViewHolder(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
            var e: Event? = null
            synchronized(eventList) {
                e = eventList.get(position)
            }

            val event: Event = e ?: Event()

            Timber.d(event.toString())

            holder.dateAndTime?.text = event.getDateTimeString()
            holder.name?.text = event.name
            holder.roomName?.text = event.location.name

            holder.container?.setOnClickListener({
                val intent = Intent(this@EventListActivity, EventDetailActivity::class.java)
                intent.putExtra(Constants.EXTRA_EVENT_ID, event.id)
                startActivity(intent)
                })
        }

        override fun getItemCount(): Int {
            synchronized(eventList) {
                return eventList.size
            }
        }
    }
}