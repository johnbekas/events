package com.dealerinspire.dievents.view

import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.dealerinspire.dievents.Constants
import com.dealerinspire.dievents.EventsApplication
import com.dealerinspire.dievents.R
import com.dealerinspire.dievents.controller.PreferenceController
import com.dealerinspire.dievents.model.Event
import com.dealerinspire.dievents.model.Session
import com.dealerinspire.dievents.utils.ImageUtils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_event_detail.*
import timber.log.Timber
import javax.inject.Inject

class EventDetailActivity : AppCompatActivity() {

    @Inject @JvmField var picasso: Picasso? = null
    @Inject @JvmField var preferenceController: PreferenceController? = null
    @Inject @JvmField var session: Session? = null

    @BindView(R.id.name) @JvmField var name: TextView? = null
    @BindView(R.id.favorite_icon) @JvmField var favoriteView: ImageView? = null
    @BindView(R.id.date_and_time) @JvmField var dateAndTime: TextView? = null
    @BindView(R.id.room_name) @JvmField var roomName: TextView? = null
    @BindView(R.id.description) @JvmField var description: TextView? = null
    @BindView(R.id.speaker_list) @JvmField var speakerListView: ViewGroup? = null

    private var event: Event = Event()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_detail)

        (this.applicationContext as EventsApplication).getApplicationComponent().inject(this)

        ButterKnife.bind(this)

        var eventId: Long? = 0

        if (savedInstanceState == null) {
            eventId = intent.getLongExtra(Constants.EXTRA_EVENT_ID, 0L)
        }

        Timber.d("Retrieving event")

        event = session?.getEvent(eventId) ?: Event()

        Timber.d(event.toString())
    }

    override fun onResume() {
        super.onResume()

        updateUI()
    }

    fun updateUI() {
        dateAndTime?.text = event.getDateTimeString()
        name?.text = event.name
        roomName?.text = String.format("Room: %s", event.location.name)
        description?.text = event.description

        speakerListView?.removeAllViews()

        val favorite = preferenceController?.isFavorite(event.id) ?: false


        updateFavoriteDrawable(favorite)

        favoriteView?.setOnClickListener({
            val isFavorite = preferenceController?.isFavorite(event.id) ?: false

            if (isFavorite) {
                preferenceController?.removeFavorite(event.id)
                updateFavoriteDrawable(false)
            } else {
                preferenceController?.addFavorite(event.id)
                updateFavoriteDrawable(true)
            }
        })

        event.speakers.forEach({
            Timber.d(it.toString())
            val view = layoutInflater.inflate(R.layout.template_speaker, speakerListView, false)
            val speakerName = view.findViewById<TextView>(R.id.name)
            val avatar = view.findViewById<ImageView>(R.id.avatar)

            speakerName.text = it.name
            picasso!!.load(it.url).into(avatar)

            speakerListView?.addView(view)
        })
    }

    fun updateFavoriteDrawable(isFavorite: Boolean) {

        Timber.d("isFavorite: %s", isFavorite);

        val favoriteDrawable: Drawable? = ImageUtils.getImageDrawable(R.drawable.ic_star_black_24dp, this@EventDetailActivity)
        val notFavoriteDrawable: Drawable? = ImageUtils.getImageDrawable(R.drawable.ic_star_border_black_24dp, this@EventDetailActivity)

        if (isFavorite) {
            favoriteView?.setImageDrawable(favoriteDrawable)
        } else {
            favoriteView?.setImageDrawable(notFavoriteDrawable)
        }
    }
}